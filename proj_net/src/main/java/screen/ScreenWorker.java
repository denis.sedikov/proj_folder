package screen;

import lombok.Setter;
import lombok.Getter;
import net.NeuralNet;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import service.GlobalKeyListener;

import java.awt.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Getter
    public class ScreenWorker {
  private   Point red;
  private   Point black;
  private   Point start;
  private   Point newBet;
    private Point validatePoint;
    private int blackColor = -10921898;
    private int redColor = -6074802;
    public ScreenWorker(int redX,int redY,
                  int blackX, int blackY,
                  int startX,int startY,
                  int okX, int okY,
                        int xValid,int yValid) {
        this.red = new Point(redX,redY);
        this.black = new Point(blackX,blackY);
        this.start = new Point(startX,startY);
        this.newBet = new Point(okX,okY);
        this.validatePoint = new Point(xValid,yValid);
    }

    public void launcher( NeuralNet net) throws InterruptedException, NativeHookException, AWTException {
        Doer doer = new Doer();

        doer.makeBet(net.getBet()/*( int ) ( Math . random () * 2)*/);
        doer.clickStart();
        doer.delay(8000);
        int result = doer.validateResult(net.getBet());
        net.setResult(result);
        doer.clickBet();
        doer.delay(500);
//        doer.delay(( int ) ( 2 + Math . random () * 5)); /*imitation people delay*/
    }



    private class Doer {
        public void clickStart() throws AWTException {
            start.mouseClick();
        }

        public void makeBet(int field) throws AWTException { /*mouse clicker*/
            if(field == 1){
                red.mouseClick();
            }
            if (field == 0){
                black.mouseClick();
            }
        }


        public int validateResult(int field) throws AWTException {
            int valueV = validatePoint.getPixelColor();
                        CalculateColor calculateColor;
            calculateColor = ( int valueLambd) -> {
                if (valueV == redColor)
                    return 1;//red
                 else
                    return 0;//black
            };
            if(calculateColor.getvalueLambd(valueV) == field){
                return 1;
            }else {
                return -1;
            }
            }

        public void delay(int sec) throws InterruptedException {
//            TimeUnit.SECONDS.sleep(sec);
        Thread.sleep(sec);
        }
        public void clickBet() throws AWTException {
            newBet.mouseClick();
        }

    }

    }

interface CalculateColor {
    int getvalueLambd(int valueLambd);
}