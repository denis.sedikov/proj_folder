package screen;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.awt.*;
import java.awt.event.InputEvent;

@Setter
@ToString
public class Point {
    final int RED_POINT_VALUE = 0;
    final int BLACK_POINT_VALUE = 1;

    int PointX ;
    int PointY ;


    public Point(int pointX, int pointY) {
        PointX = pointX;
        PointY = pointY;
    }

    public int getPixelColor() throws AWTException {
        int colorRGB;
        Robot robot = new Robot();
        // The the pixel color information at 20, 20
        Color color = robot.getPixelColor(PointX, PointY);
        colorRGB = color.getRGB();
        return colorRGB;
            }

    public void mouseClick() throws AWTException {
        Robot bot = new Robot();
        bot.mouseMove(PointX, PointY);
        bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            }
}
