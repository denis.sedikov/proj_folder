package screen;

import net.NeuralNet;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.testng.annotations.Test;
import service.GlobalKeyListener;

import java.awt.*;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class ScreenTest {
    @Test
    public void test() throws AWTException, NativeHookException, InterruptedException {
        ScreenWorker worker = new ScreenWorker(530, 472, 573, 499,
                520, 567, 450, 560, 705, 204);
        NeuralNet neuralNet = new NeuralNet();

        while (true) {
            GlobalScreen.registerNativeHook();
            GlobalScreen.addNativeKeyListener(new GlobalKeyListener());
            if (GlobalKeyListener.start == 1){
                while(true) {
                    worker.launcher(neuralNet);
                    if (GlobalKeyListener.stop == 0) {
                        GlobalKeyListener.start = 0;
                        GlobalKeyListener.stop = 1;
                        break;}
                }
            }

        }
    }


    @Test
    public void test2() throws AWTException, NativeHookException, InterruptedException {
        ScreenWorker worker = new ScreenWorker(530, 472, 573, 499,
                520, 567, 450, 560, 1135, 126);
        TimeUnit.SECONDS.sleep(4);
        System.out.println(worker.getValidatePoint().getPixelColor());

    }
}