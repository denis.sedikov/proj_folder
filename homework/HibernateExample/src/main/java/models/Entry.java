package models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Entry {
    @Column(name = "title")
    private String title;

    @Column(name = "entryText")
    private String entry;

    @Column(name = "date")
    private String date;

    @Id
    @SequenceGenerator( name = "EntrySequence", sequenceName = "ENTRY_SEQUENCE",allocationSize=3 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "EntrySequence")
    private Long id;

    @ManyToOne
    private UserN userN;

    @ManyToOne
    private Tag tag;

}

