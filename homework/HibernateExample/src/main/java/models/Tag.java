package models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Tag {
    @Column
    private String tag;

    @Id
    @SequenceGenerator( name = "tagSequence", sequenceName = "TAG_SEQUENCE", allocationSize=3)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "tagSequence")
    private Long id;

    @OneToMany(mappedBy = "entry")
    private List <Entry> entry ;
}
