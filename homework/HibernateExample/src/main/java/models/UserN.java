package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserN {
    @Id
    @SequenceGenerator( name = "userSequence", sequenceName = "USER_SEQUENCE",allocationSize=3 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "userSequence")
    private Long id;

    @Column(name ="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name ="email")
    private String email;


    @OneToMany(mappedBy = "entry")
    private List <Entry> entry ;
}
