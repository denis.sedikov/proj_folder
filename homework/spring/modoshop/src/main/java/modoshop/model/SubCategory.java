package modoshop.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class SubCategory {
    String itemMenu;
    SubCategory(String itemMenu){
        this.itemMenu = itemMenu;
    }




    public   static List<SubCategory> mainItemsWoman = new ArrayList<SubCategory>();
    static {
        mainItemsWoman.add(new SubCategory("Блузки"));
        mainItemsWoman.add(new SubCategory("Болеро"));
        mainItemsWoman.add(new SubCategory("Для офиса"));
        mainItemsWoman.add(new SubCategory("Брюки"));
        mainItemsWoman.add(new SubCategory("Верхняя одежда"));
        mainItemsWoman.add(new SubCategory("Водолазки"));
        mainItemsWoman.add(new SubCategory("Джинсы"));
        mainItemsWoman.add(new SubCategory("Жилеты"));
        mainItemsWoman.add(new SubCategory("Комбинезоны"));
        mainItemsWoman.add(new SubCategory("Костюмы"));
        mainItemsWoman.add(new SubCategory("Платья"));
        mainItemsWoman.add(new SubCategory("Пиджаки"));
        mainItemsWoman.add(new SubCategory("Жакеты"));
    }

    public   static List<SubCategory> mainItemsMan = new ArrayList<SubCategory>();
    static {
        mainItemsMan.add(new SubCategory("Брюки"));
        mainItemsMan.add(new SubCategory("Рубашки"));
        mainItemsMan.add(new SubCategory("Куртки"));
        mainItemsMan.add(new SubCategory("Джинсы"));
        mainItemsMan.add(new SubCategory("Верхняя одежда"));
        mainItemsMan.add(new SubCategory("Водолазки"));
        mainItemsMan.add(new SubCategory("Жилеты"));
        mainItemsMan.add(new SubCategory("Костюмы"));
        mainItemsMan.add(new SubCategory("Пиджаки"));
    }
    public   static Map<String, List<SubCategory>> subCategories = new HashMap<String, List<SubCategory>>();
    static {
        subCategories.put(Category.items.get(0).catAsText(), mainItemsWoman);
        subCategories.put(Category.items.get(1).catAsText(), mainItemsMan);
    }
}