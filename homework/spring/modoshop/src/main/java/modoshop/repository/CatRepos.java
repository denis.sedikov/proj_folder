package modoshop.repository;

import modoshop.entity.Categories;
import org.springframework.data.repository.CrudRepository;

public interface CatRepos extends CrudRepository<Categories, Long> {
}
