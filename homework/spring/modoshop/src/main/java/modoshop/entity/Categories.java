package modoshop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "categories")
public class Categories {
    @Id
    private Long id;
    private String parentcat;
    private String catname;
    @OneToMany
    @JoinTable(name = "cat2item",
            joinColumns = {@JoinColumn(name = "id_cat")},
            inverseJoinColumns = {@JoinColumn(name = "id")})
    private List<Item> globalItems;
}
