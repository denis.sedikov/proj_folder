package modoshop.controller;

import modoshop.model.Category;
import modoshop.model.SubCategory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class NavMenuController {
    @GetMapping("/category/{items}")
    public String category(@PathVariable String items, Model model) {

        model.addAttribute("NavItems", Category.items);
        model.addAttribute("LeftNavItems",SubCategory.subCategories.get(items));
        return "category";

    }
}