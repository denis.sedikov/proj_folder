package com.home.work.service.impl;

import com.home.work.service.ChooseService;
import com.home.work.service.LaunchService;
import com.home.work.service.TimerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class LaunchServiceImpl implements LaunchService {
    @Autowired
    ChooseService chooseService;
    @Resource
    TimerService timerService;

    public LaunchServiceImpl() {
        System.out.println("Hello! Im launcher(Service).  called and  initialized");
    }

    public void launcher(int time, long id) {

        final String application = chooseService.chooseApp(id);

        timerService.timerOn(time);
        System.out.println("The " + application + " running.  (LaunchService)"+"\n");
    }

    public void launcher(long id) {
        final String application = chooseService.chooseApp(id);
        timerService.noTimer();
        System.out.println("The " + application + " running"+"\n");
    }
    public void wtf(){
        System.out.println("Wtf! it not empty object");
    }

    public ChooseService getChooseService() {
        return chooseService;
    }

    public void setChooseService(ChooseService chooseService) {
        this.chooseService = chooseService;
    }

    public TimerService getTimerService() {
        return timerService;
    }

    public void setTimerService(TimerService timerService) {
        this.timerService = timerService;
    }
}