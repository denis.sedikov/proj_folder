package com.home.work.service.impl;

import com.home.work.service.ConstrArgService;
import com.home.work.service.MessageService;
import org.springframework.stereotype.Component;

@Component
public class MessageServiceImpl implements MessageService {
    private static int id = 0;
    String message;

    public MessageServiceImpl(ConstrArgService constrArgService) {
     id++;
     System.out.println("MessageServiceImpl. called and  initialized");
    }

    public MessageServiceImpl() {
        id++;
        System.out.println("MessageServiceImpl. called and  initialized");

    }


    public void getMessage(String message){
        System.out.println(message+". object id = "+id);
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
